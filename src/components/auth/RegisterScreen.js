import React from 'react'
import { Link } from 'react-router-dom'
import { useForm } from '../../hooks/useForm'
import validator from "validator";
import { useDispatch, useSelector } from 'react-redux';
import { removeError, setError } from '../../actions/ui';
import { startRegisterWithEmailPasswordName } from '../../actions/auth';

export const RegisterScreen = () => {

  const dispatch = useDispatch();
  const {msgError} = useSelector(state => state.ui);

  const [formValues, handleInputChange] = useForm({
    name: '',
    email: '',
    password: '',
    password2: ''
  });

  const {name, email, password, password2} = formValues;

  const handleRegister = (e) => {
    e.preventDefault();
    
    if (isFormValid()) {
      dispatch(startRegisterWithEmailPasswordName(email, password, name));
    }
  }

  const isFormValid = () => {

    if (name.trim().length === 0) {
      dispatch(setError('Name is required'));
      return false;
    }

    if (!validator.isEmail(email)) {
        dispatch(setError('Email is not valid'));
        return false;
    }

    if (password !== password2 || password.length < 5) {
      dispatch(setError('Password should be at leaast 6 characters and match each other'));
      return false;
    }

    dispatch(removeError());

    return true;
  }

  return (
    <>
        <h3 className='auth__title'>Register</h3>

        <form onSubmit={handleRegister}
          className="animate__animated animate__fadeIn animate__faster"
        >

          {
            msgError &&
            (
              <div className='auth__alert-error'>
                {msgError}
              </div>
            )
          }

          <input 
            type="text"
            placeholder="Name"
            className='auth__input'
            autoComplete='off'
            name='name'
            value={name}
            onChange={handleInputChange}
          />

          <input 
            type="text"
            placeholder="Email"
            className='auth__input'
            autoComplete='off'
            name='email'
            value={email}
            onChange={handleInputChange}
          />

          <input 
            type="password"
            placeholder="Password"
            className='auth__input'
            name='password'
            value={password}
            onChange={handleInputChange}
          />

          <input 
            type="password"
            placeholder="Confirm Password"
            className='auth__input'
            name='password2'
            value={password2}
            onChange={handleInputChange}
          />

          <button 
            type='text'
            className='btn btn-primary btn-block mb-5'
          >
            Register
          </button>
          

          <Link to="/auth/login"
            className='link'  
          >
            Already register?
          </Link>
        </form>
    </>
  )
}
