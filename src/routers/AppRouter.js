import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { login } from '../actions/auth'
import {  startLoadingNotes } from '../actions/notes'
import { JournalScreen } from '../components/journal/JournalScreen'
import { firebase } from '../firebase/firebaseConfig'
import { AuthRouter } from './AuthRouter'
import { PrivateRoutes } from './PrivateRoutes'
import { PublicRoutes } from './PublicRoutes'

export const AppRouter = () => {

  const dispatch = useDispatch();

  const [checking, setChecking] = useState(true);
  const [isLoggedIn, setIsLoggedIn] = useState(false);


  useEffect(() => {
    firebase.auth().onAuthStateChanged( async (user) => {
      
      if (user?.uid) {
        dispatch(login(user.uid, user.displayName));
        setIsLoggedIn(true);

        dispatch(startLoadingNotes(user.uid));

      } else {
        setIsLoggedIn(false);
      }

      setChecking(false);

    });
  }, [dispatch, setChecking, setIsLoggedIn])

  if (checking) {
    return (
      <h1>Wait...</h1>
    );
  }
  
  return (
    <BrowserRouter>
        <Routes>
            <Route path='/auth/*' element={
              <PublicRoutes isAuth={isLoggedIn}>
                <AuthRouter />
              </PublicRoutes>
            } />
            <Route path='/' element={
              <PrivateRoutes isAuth={isLoggedIn}>
                <JournalScreen />
              </PrivateRoutes>
            } />
        </Routes>
    </BrowserRouter>
  )
}
