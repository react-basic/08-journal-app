/**
 * @jest-environment node
 */
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { startUploading } from '../../actions/notes';
import { fileUpload } from "../../helpers/fileUpload";
import * as fs from 'fs';
import { db } from '../../firebase/firebaseConfig';

jest.mock("../../helpers/fileUpload", () => ({
    fileUpload: jest.fn()
}))

global.scrollTo = jest.fn();

const middlewares = [thunk]
const mockStore = configureStore(middlewares)


const initState = {
    auth: {
        uid: 'TESTING'
    },
    notes: {
        active: {
            id: 'N88Ru4uVLlWYtfHnUzJb',
            title: 'hola',
            body: 'mundo'
        }
    }
};

let store = mockStore(initState);



describe('Pruebas con las acciones de notes2', () => {

    beforeEach( () => {
        store = mockStore(initState);
    })

    test('startUploading debe de actualizar el url del entry', async () => {

        fileUpload.mockReturnValue('https://hola-mundo.com/foto.jpg');

        fs.writeFileSync('foto.jpg', '')
        const file = fs.readFileSync('foto.jpg')
        await store.dispatch(startUploading(file));

        const docRef = await db.doc(`/TESTING/journal/notes/N88Ru4uVLlWYtfHnUzJb`).get();
        expect(docRef.data().url).toBe('https://hola-mundo.com/foto.jpg');

    })

})