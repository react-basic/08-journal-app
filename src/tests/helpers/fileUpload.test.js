import 'setimmediate';
import { fileUpload } from "../../helpers/fileUpload";
import cloudinary from 'cloudinary'


cloudinary.config({ 
  cloud_name: 'dwxcgvmtl', 
  api_key: '192394571349794', 
  api_secret: 'JCNAnMkzi4VPRZCIw71OBzwhURc',
});


describe('Pruebas en fileUpload', () => {

    test('debe de cargar un archivo y retornar el URL', async () => {

        const resp = await fetch('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRRuHyJwUb0dULxYhnFUi2GBQXFitTklMypD2fVnd6_UV0W6n7AhiSw4umcwDUBtIQhCFo&usqp=CAU');
        const blob = await resp.blob();

        const file = new File([blob], 'foto.png');
        const url = await fileUpload(file);
        console.log(url);

        expect(typeof url).toBe('string');

        // Borrar imagen por ID
        const segments = url.split('/');
        const imageId = segments[segments.length -1].replace('.jpg', '');
        const {deleted} = await cloudinary.v2.api.delete_resources(imageId);
        
        expect(deleted).toEqual({ [imageId]: "deleted" });

    })

    test('debe de retornar un error', async () => {

        const file = new File([], 'foto.png');
        const url = await fileUpload(file);

        expect(url).toBe(null);

    })

})