import { types } from "../../types/types"


describe('Pruebas en types.js', () => {

    test('debe de mostrar los parametros de types correctamente', () => {

        const typesExpected = {
            login: '[Auth] login',
            logout: '[Auth] logout',
        
            uiSetError: '[UI] set error',
            uiRemoveError: '[UI] remove error',
            
            uiStartLoading: '[UI] Start loading',
            uiFinishLoading: '[UI] Finish loading',
        
            notesAddNew: '[Notes] New note',
            notesActive: '[Notes] Set Active note',
            notesLoad: '[Notes] Load notes',
            notesUpdated: '[Notes] Updated note saved',
            notesFileUrl: '[Notes] Updated image url',
            notesDelete: '[Notes] Delete note',
            notesLogoutCleaning: '[Notes] Logout Cleaning',
        }

        expect(types).toEqual(typesExpected);

    })

})