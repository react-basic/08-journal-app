import { authReducer } from "../../reducers/authReducer"
import { types } from "../../types/types"



describe('Pruebas en authReducer', () => {


    test('debe responder el state del login', () => {

        const action = {
            type: types.login,
            payload: {
                uid: 'ABC',
                displayName: 'test'
            }
        }

        const state = authReducer({}, action);

        expect(state.uid).toBe(action.payload.uid);
        expect(state.name).toBe(action.payload.displayName);
    })

    test('debe responder el state del logout vacio', () => {

        const action = {
            type: types.logout,
        }

        const state = authReducer({
            uid: 'ABC', 
            displayName: 'test'
        }, action);

        expect(state).toEqual({});
    })

    test('debe responder el state por default', () => {

        const action = {
            type: 'asdasdsa',
        }

        const state = authReducer({
            uid: 'ABC', 
            displayName: 'test'
        }, action);

        expect(state).toEqual({
            uid: 'ABC', 
            displayName: 'test'
        });
    })

})